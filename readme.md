# søppel

pase code and share it!

## Introduction

Søppel is a simplified service for sharing code examples or other texts.

## Formats

Søppel supports the following formats for syntax highlighting.

```
Plaintext, Access logs, ActionScript, Apache, AppleScript, Bash, Basic, Brainfuck, 
C#, C, C++, CSS, CoffeeScript, Dart, Diff, Django, Dockerfile, Dust, Erlang, Go, 
HTML, XML, Haskell, Ini, JSON, Java, JavaScript, Kotlin, LaTeX, Less, Makefile, 
Markdown, Matlab, Nginx, PHP, Perl, PostgreSQL, PowerShell, Python, Ruby, Rust, 
SCSS, SQL, Shell, Swift, Twig, TypeScript, VB.Net, VBScript, YAML
```

## Docker

### Services

| Service | Name | Internal Port | Url |
| --- | --- | --- | --- |
| Webserver | webserver | 80 | http://localhost:8010 |
| php-fpm 7.4 | php-fpm | - | - |

### Installation

You only need to start the Docker containers with

```
docker-compose up -d
```

After that you need to run `composer install` in the `php-fpm` container. Use this command:

```
docker-compose run --rm php-fpm composer install
```

### Shorts

Open a new php-fpm container to run commands or anything else.

```
docker-compose run --rm php-fpm /bin/bash
```

## Style

To rebuild the style there are 2 options.

First run

```
npm install
```

### watch

Run this command to rebuild the style on every change. Perfect for local work.

```
npm run watch
```

### build

Before you push a new style, run this command.

```
npm run build
```
