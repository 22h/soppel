<?php

namespace App\Tests;

use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class IntegrationTestCase extends WebTestCase
{

    protected function overrideService(string $className, MockObject $mockObject): void
    {
        $this->getContainer()->set('test.' . $className, $mockObject);
    }
}
