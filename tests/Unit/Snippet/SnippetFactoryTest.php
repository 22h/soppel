<?php

namespace App\Tests\Unit\Snippet;

use App\Snippet\Form\CreateSnippetRequestData;
use App\Snippet\SnippetFactory;
use App\Tests\UnitTestCase;

class SnippetFactoryTest extends UnitTestCase
{

    public function testCreateSnippetByRequestData(): void
    {
        $format = 'php';
        $text = '<?php echo $test; ?>';

        $requestData = (new CreateSnippetRequestData())->setText($text)->setFormat($format);

        $snippetFactory = new SnippetFactory();
        $snippet = $snippetFactory->createByRequestData($requestData);

        $this->assertSame($format, $snippet->getFormat());
        $this->assertSame($text, $snippet->getText());
        $this->assertStringEndsWith('-' . $format, $snippet->getIdent());
        $this->assertGreaterThan(64, strlen($snippet->getIdent()));
    }
}
