<?php

namespace App\Tests\Unit\Snippet;

use App\Snippet\Snippet;
use App\Tests\UnitTestCase;

class SnippetTest extends UnitTestCase
{

    public function testSnippet(): void
    {
        $ident = 'example-plain';
        $text = 'example text';
        $format = 'plain';

        $snippet = new Snippet($ident, $text, $format);

        $this->assertSame($ident, $snippet->getIdent());
        $this->assertSame($text, $snippet->getText());
        $this->assertSame($format, $snippet->getFormat());
    }
}
