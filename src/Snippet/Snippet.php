<?php

namespace App\Snippet;

class Snippet
{

    public function __construct(
        private string $ident,
        private string $text,
        private string $format
    ) {
    }

    public function getIdent(): string
    {
        return $this->ident;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function getFormat(): string
    {
        return $this->format;
    }
}
