<?php

namespace App\Snippet;

use App\Snippet\Form\CreateSnippetRequestData;

class SnippetFactory
{

    public function createByRequestData(CreateSnippetRequestData $requestData): Snippet
    {
        return $this->newInstance(
            $this->createIdent($requestData),
            $requestData->getText(),
            $requestData->getFormat()
        );
    }

    private function createIdent(CreateSnippetRequestData $requestData): string
    {
        return implode('', [
            hash('sha256', $requestData->getText() . $requestData->getFormat() . time()),
            '-',
            $requestData->getFormat(),
        ]);
    }

    private function newInstance(string $ident, string $text, string $format): Snippet
    {
        return new Snippet($ident, $text, $format);
    }
}
