<?php

namespace App\Snippet;

use App\Snippet\Form\CreateSnippetRequestData;

class SnippetService
{

    public function __construct(private SnippetFactory $factory, private SnippetRepository $repository)
    {
    }

    public function createByRequestData(CreateSnippetRequestData $requestData): Snippet
    {
        $snippet = $this->factory->createByRequestData($requestData);

        $this->repository->persistSnippet($snippet);

        return $snippet;
    }

    public function findContentByIdent(string $ident): ?string
    {
        return $this->repository->findSnippetContent($ident);
    }

    public function getFormatByIdent(string $ident): string
    {
        return substr($ident, strrpos($ident, '-') + 1);
    }
}
