<?php

namespace App\Snippet\Form;

use App\Snippet\SnippetFormat;
use App\Snippet\Validation\PossibleSnippetFormat;
use Symfony\Component\Validator\Constraints as Assert;

class CreateSnippetRequestData
{
    /**
     * @Assert\NotBlank()
     */
    private ?string $text = null;

    /**
     * @PossibleSnippetFormat()
     * @Assert\NotBlank()
     */
    private ?string $format = SnippetFormat::DEFAULT_FORMAT;

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(?string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getFormat(): ?string
    {
        return $this->format;
    }

    public function setFormat(?string $format): self
    {
        $this->format = $format;

        return $this;
    }
}
