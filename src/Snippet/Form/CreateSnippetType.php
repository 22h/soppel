<?php

namespace App\Snippet\Form;

use App\Snippet\SnippetFormat;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CreateSnippetType extends AbstractType
{

    private const REQUIRED = 'required';

    public function __construct(private SnippetFormat $snippetFormat)
    {
    }

    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add(
            'text',
            TextareaType::class,
            [self::REQUIRED => true]
        )->add(
            'format',
            ChoiceType::class,
            [
                'choices' => array_flip($this->snippetFormat->getAll()),
            ]
        )->add(
            'send',
            SubmitType::class,
            [
                'label' => 'Speichern',
            ]
        );
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            [
                'data_class' => CreateSnippetRequestData::class,
                'csrf_protection' => true,
            ]
        );
    }
}
