<?php

namespace App\Snippet\Validation;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class PossibleSnippetFormat extends Constraint
{
    public string $message = 'format is not valid';
}
