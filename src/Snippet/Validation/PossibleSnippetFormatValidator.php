<?php

namespace App\Snippet\Validation;

use App\Snippet\SnippetFormat;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class PossibleSnippetFormatValidator extends ConstraintValidator
{

    public function __construct(private SnippetFormat $snippetFormat)
    {
    }

    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof PossibleSnippetFormat) {
            throw new UnexpectedTypeException($constraint, PossibleSnippetFormat::class);
        }

        if (null === $value || '' === $value) {
            return;
        }

        if (!array_key_exists($value, $this->snippetFormat->getAll())) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }
}
