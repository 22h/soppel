<?php

namespace App\Snippet;

use Symfony\Component\Filesystem\Filesystem;

class SnippetRepository
{

    public function __construct(private string $snippetDir, private Filesystem $filesystem)
    {
    }

    public function hasSnippet(string $ident): bool
    {
        return $this->filesystem->exists($this->getFullPathBySnippetIdent($ident));
    }

    public function findSnippetContent(string $ident): ?string
    {
        if (!$this->hasSnippet($ident)) {
            return null;
        }

        $content = file_get_contents($this->getFullPathBySnippetIdent($ident));
        if (is_string($content)) {
            return $content;
        }

        return null;
    }

    public function removeSnippet(string $ident): void
    {
        $this->filesystem->remove($this->getFullPathBySnippetIdent($ident));
    }

    public function persistSnippet(Snippet $snippet): void
    {
        $this->filesystem->mkdir($this->getPathBySnippetIdent($snippet->getIdent()));
        $this->filesystem->dumpFile($this->getFullPathBySnippetIdent($snippet->getIdent()), $snippet->getText());
    }

    private function getFullPathBySnippetIdent(string $ident): string
    {
        return $this->getPathBySnippetIdent($ident) . DIRECTORY_SEPARATOR . $this->getFileNameBySnippetIdent($ident);
    }

    private function getPathBySnippetIdent(string $ident): string
    {
        $partials = str_split($ident, 2);

        $pathPartials = [
            $this->snippetDir,
            $partials[0],
            $partials[1],
            $partials[2],
        ];

        return implode(DIRECTORY_SEPARATOR, $pathPartials);
    }

    private function getFileNameBySnippetIdent(string $ident): string
    {
        return $ident . '.txt';
    }
}
