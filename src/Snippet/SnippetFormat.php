<?php

namespace App\Snippet;

class SnippetFormat
{

    public const DEFAULT_FORMAT = 'plain';

    /**
     * @return string[]
     */
    public function getAll(): array
    {
        return [
            'plain' => 'Plaintext',
            'accesslog' => 'Access logs',
            'actionscript' => 'ActionScript',
            'apacheconf' => 'Apache',
            'applescript' => 'AppleScript',
            'bash' => 'Bash',
            'basic' => 'Basic',
            'brainfuck' => 'Brainfuck',
            'csharp' => 'C#',
            'c' => 'C',
            'cpp' => 'C++',
            'css' => 'CSS',
            'coffeescript' => 'CoffeeScript',
            'dart' => 'Dart',
            'diff' => 'Diff',
            'django' => 'Django',
            'dockerfile' => 'Dockerfile',
            'dust' => 'Dust',
            'erlang' => 'Erlang',
            'go' => 'Go',
            'xml' => 'HTML, XML',
            'haskell' => 'Haskell',
            'ini' => 'Ini',
            'json' => 'JSON',
            'java' => 'Java',
            'javascript' => 'JavaScript',
            'kotlin' => 'Kotlin',
            'tex' => 'LaTeX',
            'less' => 'Less',
            'makefile' => 'Makefile',
            'markdown' => 'Markdown',
            'matlab' => 'Matlab',
            'nginx' => 'Nginx',
            'php' => 'PHP',
            'perl' => 'Perl',
            'pgsql' => 'PostgreSQL',
            'powershell' => 'PowerShell',
            'python' => 'Python',
            'ruby' => 'Ruby',
            'rust' => 'Rust',
            'scss' => 'SCSS',
            'sql' => 'SQL',
            'shell' => 'Shell',
            'swift' => 'Swift',
            'twig' => 'Twig',
            'ts' => 'TypeScript',
            'vbnet' => 'VB.Net',
            'vbscript' => 'VBScript',
            'yml' => 'YAML',
        ];
    }
}
