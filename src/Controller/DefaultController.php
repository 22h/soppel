<?php

namespace App\Controller;

use App\Snippet\Form\CreateSnippetRequestData;
use App\Snippet\Form\CreateSnippetType;
use App\Snippet\SnippetService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    public function __construct(private SnippetService $snippetService)
    {
    }

    /**
     * @Route("/", name="app_home")
     */
    public function home(
        Request $request,
        SnippetService $snippetService
    ): Response {
        $requestData = new CreateSnippetRequestData();

        $form = $this->createForm(CreateSnippetType::class, $requestData);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var CreateSnippetRequestData $requestData */
            $requestData = $form->getData();

            $snippet = $snippetService->createByRequestData($requestData);

            return $this->redirectToRoute('app_snippet', ['ident' => $snippet->getIdent()]);
        }

        return $this->render(
            'home.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/s/{ident}", name="app_snippet")
     */
    public function revision(string $ident): Response
    {
        $snippetContent = $this->snippetService->findContentByIdent($ident);
        $format = $this->snippetService->getFormatByIdent($ident);

        if (is_null($snippetContent)) {
            $snippetContent = '-404: Sorry, but we couldn\'t find anything.';
            $format = 'diff';
        }

        return $this->render(
            'snippet.html.twig',
            [
                'snippetContent' => $snippetContent,
                'format' => $format,
            ]
        );
    }
}
